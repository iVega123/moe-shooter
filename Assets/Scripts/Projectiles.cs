﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectiles : MonoBehaviour
{// Class responsible for the bullets
    private const string BULLET_PREFAB_PATH = "Prefabs/Bullet";
    public float bulletSpeed = 10000.0F;
    private GameObject bulletSpawn;
    public float timeToLive = 2.0F;
    void Start()
    {// Find the point where the bullet should come out ...
        bulletSpawn = GameObject.Find("ShootMuzzle");
    }
    public void Shoot()
    {// Function responsible for triggering the shooting condition
        var tempBullet = ObjectPooler.GetPooledObject(BULLET_PREFAB_PATH);
        tempBullet.transform.position = bulletSpawn.transform.position;
        tempBullet.transform.rotation = bulletSpawn.transform.rotation;

        tempBullet.SetActive(true);
    }
}
