﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGenerator : MonoBehaviour
{// Class responsible for the procedural generation of the Tree according to what is set as minimum and maximum in the editor
    #region Attributes
    public int minPieces;
    public int maxPieces;
    public GameObject[] trunk;
    public float compesationsize = 5.9F;
    #endregion
    public void Start()
    {
        Build();
    }
    #region ProceduralBuild
    void Build()
    {// Construction of the tree taking the minimum and maximum values ...
        int targetPieces = Random.Range(minPieces, maxPieces);
        float heighOffset = 0;
        heighOffset += SpawnPiecesLayer(trunk, heighOffset);
        for (int i=2; i < targetPieces; i++)
        {
            heighOffset += SpawnPiecesLayer(trunk, heighOffset);
        }
        SpawnPiecesLayer(trunk, heighOffset);
    }
    float SpawnPiecesLayer(GameObject[] pieceArray, float inputHeight)
    {// if there were more than one GameObject these people would be combined and would be part of the construction this class is responsible for assembling the tree
        Transform randomTransform = pieceArray[Random.Range(0, pieceArray.Length)].transform;
        GameObject clone = Instantiate(randomTransform.gameObject, this.transform.position 
            + new Vector3(0,inputHeight,0), transform.rotation)as GameObject;
        Mesh cloneMesh = clone.GetComponentInChildren<MeshFilter>().mesh;
        Bounds baseBounds = cloneMesh.bounds;
        float heightOffset = baseBounds.size.y - compesationsize;

        clone.transform.SetParent(this.transform);

        return heightOffset;

    }
    #endregion
}
