﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour{
    #region Attributes
    // Attributes needed to create the Pooler
    private const int defaultPoolSize = 30;

    public static bool expandWhenNecessary = true;

    public static Dictionary<string, List<GameObject>> objectpools = new Dictionary<string, List<GameObject>>();
    #endregion

    #region Object Pool Checks
    private static bool PoolExistsForPrefab(string prefabPath)
    {// Check if the Pool exists for the prefab itself ...
        return objectpools.ContainsKey(prefabPath);
    }

    private static bool IsAvailableForReuse(GameObject gameObject)
    {// Check if the gameObject is available for reuse
        return !gameObject.activeSelf;
    }
    #endregion

    #region Object Pool Creation
    private static GameObject ExpandPoolAndGetObject(string prefabPath, List<GameObject> pool)
    {// Expand the Pool if necessary
        if (!expandWhenNecessary) return null;

        GameObject prefab = Resources.Load<GameObject>(prefabPath);

        GameObject instance = Object.Instantiate(prefab) as GameObject;

        pool.Add(instance);

        return instance;
    }

    public static List<GameObject> CreateObjectPool(string prefabPath, int Count)
    {// Create the Object Pool according to the prefab
        if (Count <= 0) Count = 1;

        GameObject prefab = Resources.Load<GameObject>(prefabPath);

        List<GameObject> objects = new List<GameObject>();

        for (int i = 0; i < Count; i++)
        {
            GameObject instance = Object.Instantiate<GameObject>(prefab);

            objects.Add(instance);

            instance.SetActive(false);
        }
        objectpools.Add(prefabPath, objects);

        return objects;
    }

    #endregion

    #region Object Retrival

    public static GameObject GetPooledObject(string prefabPath, int poolSize = defaultPoolSize)
    {// Get the object inside the Pool
        if (!PoolExistsForPrefab(prefabPath))
        {
            return CreateAndRetrieveFromPool(prefabPath, poolSize);
        }
        var pool = objectpools[prefabPath];

        GameObject instance;
        return (instance = FindFirstAvailablePooledObject(pool)) != null ?
            instance : ExpandPoolAndGetObject(prefabPath, pool);
    }

    private static GameObject CreateAndRetrieveFromPool(string prefabPath, int poolSize = defaultPoolSize)
    {// Create and remove from the Pool
        CreateObjectPool(prefabPath, poolSize);
        return GetPooledObject(prefabPath);
    }

    private static GameObject FindFirstAvailablePooledObject(List<GameObject> pool)
    {// Find the first object that can be used in the Pool
        for (int i = 0; i<pool.Count; i++)
        {
            if (IsAvailableForReuse(pool[i]))
            {
                return pool[i];
            }
        }

        return null;
    }

    #endregion
}

