﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilesBehaviour : MonoBehaviour
{// Class responsible for handling the bullet after being fired
    #region Attributes
    public float unitsPerSecond = 1000;

    public float lifetime = 4;
    #endregion
    private void Start()
    {// Calls the Destroy function responsible for leaving the bullet inactive after some time
        Invoke("Destroy", lifetime);
    }

    // Calculation of Bullet Speed and Direction
    private void Update()
    {
        var forwardDir = transform.InverseTransformDirection(transform.up);
        transform.Translate(forwardDir * unitsPerSecond * Time.deltaTime);
    }

    private void Destroy()
    {
        gameObject.SetActive(false);
    }
}
