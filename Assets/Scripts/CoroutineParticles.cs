﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Not Implemented...
public class CoroutineParticles : MonoBehaviour
{
    #region Attributes
    [SerializeField] Vector3 startPosition;
    [SerializeField] Vector3 endPosition;
    [SerializeField] float speed;
    Animator animator;
    #endregion
    void Start()
    {// Instance of Animator and Coroutine
        animator = GetComponent<Animator>();
        StartCoroutine(Animations(startPosition));
    }
    #region Coroutines
    IEnumerator Animations(Vector3 target)
    {// Where Coroutine should trigger the pre-settled bools in the Animator ...
        while (Mathf.Abs((target-transform.position).x)>0.20f)
        {
            Vector3 direction = target.x == startPosition.y ? Vector3.left : Vector3.right;
            animator.SetBool("spawn", true);
            animator.SetBool("Idle", false);
            transform.position -= direction * Time.deltaTime * speed;
            yield return null;
        }
        yield return new WaitForSeconds(2);
        Vector3 newTarget = target.x == startPosition.y ? endPosition : startPosition;
        StartCoroutine(Animations(newTarget));
    }
    #endregion
}
