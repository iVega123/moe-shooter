﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTree : MonoBehaviour
{// Class responsible for destroying the object and creating the effect of the cubes
    #region Attributes
    public float cubeSize = 0.2f;
    public int cubesInRow = 5;
    float cubesPivotDistance;
    public float ExplosionForce = 50;
    public float ExplosionRadius = 4;
    public float ExplosionUpward = 0.4F;
    Vector3 cubesPivot;
    #endregion
    void Start()
    {// Instance of the cubes ..
        cubesPivotDistance = cubeSize * cubesInRow / 2;

        cubesPivot = new Vector3(cubesPivotDistance, cubesPivotDistance, cubesPivotDistance);
    }
    #region Explosion Logic
    public void explode()
    {// Class responsible for creating the Cubes explosion
        Destroy(transform.parent.gameObject);

        for (int x = 0; x < cubesInRow; x++)
        {
            for (int y = 0; y < cubesInRow; y++)
            {
                for (int z = 0; z < cubesInRow; z++)
                {
                    createPiece(x, y, z);
                }
            }
        }

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, ExplosionRadius);

        foreach(Collider hit in colliders)
        {
            hit.enabled = true;
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(ExplosionForce, transform.position, ExplosionRadius, ExplosionUpward);
                hit.enabled = false;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bullet")
        {
            explode();
        }
    }
    void createPiece(int x, int y, int z)
    {// Method to create the cubes and destroy them after some time
        GameObject piece;
        piece = GameObject.CreatePrimitive(PrimitiveType.Cube);
        piece.transform.position = transform.position + new Vector3(cubeSize * x, cubeSize * y, cubeSize * z) - cubesPivot;
        piece.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);
        piece.AddComponent<Rigidbody>();
        piece.GetComponent<Rigidbody>().mass = cubeSize;
        Destroy(piece, Random.Range(0.2f, 2f));
    }
    void OnDestroy()
    {// Destroy the Generate Tree component when it reaches 0
        var a = transform.parent.parent;
        if(a.childCount == 1)
        {
            Destroy(a.gameObject);
        }
    }
    #endregion
}
