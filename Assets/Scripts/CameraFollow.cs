﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{// Get the object set by the editor to follow
    #region Attributes
    public Transform objects;

    public float smoothSpeed = 0.125f;

    public Vector3 offset;
    #endregion
    void FixedUpdate()
    {// Adjust the Smooth of the camera and the position in relation to the Object
        Vector3 desiredPosition = transform.position = objects.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
        
    }
}
